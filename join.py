# joining two tables 
def join():
    cp.rename(columns={'VIEW_COUPON_ID_hash':'COUPON_ID_hash'},inplace=True)#renaming the column name
    inn = pd.merge(cp[['I_DATE','COUPON_ID_hash','USER_ID_hash']],cd[['PURCHASEID_hash','ITEM_COUNT','COUPON_ID_hash']],
                   on='COUPON_ID_hash',how = 'inner')
    print("****After Inner joining****",'\n','\n',inn,'\n')
    out = pd.merge(cp,cd[['ITEM_COUNT','PURCHASEID_hash','USER_ID_hash']],
                   on = 'USER_ID_hash',how='outer')
    print("****After outer joining****",'\n','\n',out,'\n')

    print(inn.isna().sum())
    print(out.isna().sum())
    #print(inn['I_DATE'].value_counts)
    #print(inn['PURCHASE_FLG'].value_counts())
    gp = inn.groupby('USER_ID_hash')['COUPON_ID_hash', 'I_DATE','PURCHASEID_hash'].head(10)#grouping the columns
    print(gp)
join()
