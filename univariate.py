def uni():
    data.hist(bins=50, color='blue')
    data.plot(kind='density', subplots=True, layout=(3, 3), sharex=False)
    pd.plotting.scatter_matrix(data, alpha=0.3, figsize=(14, 8), diagonal='kde');
    plt.show()
uni()